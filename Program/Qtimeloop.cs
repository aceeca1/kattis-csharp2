﻿using System;

class Qtimeloop {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 1; i <= n; ++i) {
            Console.WriteLine("{0} Abracadabra", i);
        }
    }
}
