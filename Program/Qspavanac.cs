﻿using System;

class Qspavanac {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var hour = int.Parse(line[0]);
        var minute = int.Parse(line[1]) - 45;
        if (minute < 0) {
            minute += 60;
            --hour;
        }
        if (hour < 0) hour += 24;
        Console.WriteLine("{0} {1}", hour, minute);
    }
}