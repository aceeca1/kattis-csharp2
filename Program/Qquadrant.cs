﻿using System;

class Qquadrant {
    public static void Main() {
        int x = int.Parse(Console.ReadLine());
        int y = int.Parse(Console.ReadLine());
        if (x < 0) {
            Console.WriteLine(y < 0 ? 3 : 2);
        } else {
            Console.WriteLine(y < 0 ? 4 : 1);
        }
    }
}
