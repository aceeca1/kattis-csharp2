﻿using System;

class Qr2 {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var a1 = int.Parse(line[0]);
        var a2 = int.Parse(line[1]);
        Console.WriteLine(a2 + a2 - a1);
    }
}