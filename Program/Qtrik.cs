﻿using System;

class Qtrik {
    public static void Main() {
        var hat = 0;
        foreach (char i in Console.ReadLine()) {
            switch (i) {
                case 'A': hat = hat == 0 ? 1 : hat == 1 ? 0 : 2; break;
                case 'B': hat = hat == 1 ? 2 : hat == 2 ? 1 : 0; break;
                case 'C': hat = hat == 2 ? 0 : hat == 0 ? 2 : 1; break;
            }
        }
        Console.WriteLine(hat + 1);
    }
}