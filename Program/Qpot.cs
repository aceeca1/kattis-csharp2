﻿using System;

class Qpot {
    public static void Main() {
        var answer = 0.0;
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine();
            var a = int.Parse(line.Substring(0, line.Length - 1));
            var b = line[line.Length - 1] - '0';
            answer += Math.Pow(a, b);
        }
        Console.WriteLine("{0:F0}", answer);
    }
}
