﻿using System;

class Qspeedlimit {
    public static void Main() {
        while (true) {
            var n = int.Parse(Console.ReadLine());
            if (n == -1) return;
            var total = 0;
            var last = 0;
            for (int i = 0; i < n; ++i) {
                var line = Console.ReadLine().Split();
                var mph = int.Parse(line[0]);
                var elapsed = int.Parse(line[1]);
                total += (elapsed - last) * mph;
                last = elapsed;
            }
            Console.WriteLine("{0} miles", total);
        }
    }
}