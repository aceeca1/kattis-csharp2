﻿using System;

class Qqaly {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        var answer = 0.0;
        for (int i = 0; i < n; ++i) {
            var line = Console.ReadLine().Split();
            var a1 = double.Parse(line[0]);
            var a2 = double.Parse(line[1]);
            answer += a1 * a2;
        }
        Console.WriteLine("{0:F3}", answer);
    }
}