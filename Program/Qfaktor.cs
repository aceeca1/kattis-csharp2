﻿using System;

class Qfaktor {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var article = int.Parse(line[0]);
        var factor = int.Parse(line[1]);
        Console.WriteLine(article * (factor - 1) + 1);
    }
}
