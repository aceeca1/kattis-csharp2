﻿using System;

class Qsibice {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var n = int.Parse(line[0]);
        var w = int.Parse(line[1]);
        var h = int.Parse(line[2]);
        var m = w * w + h * h;
        for (int i = 0; i < n; ++i) {
            var k = int.Parse(Console.ReadLine());
            Console.WriteLine(k * k <= m ? "DA" : "NE");
        }
    }
}
