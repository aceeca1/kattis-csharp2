﻿using System;

class Qtarifa {
    public static void Main() {
        var x = int.Parse(Console.ReadLine());
        var n = int.Parse(Console.ReadLine());
        var remain = x;
        for (int i = 0; i < n; ++i) {
            remain += x - int.Parse(Console.ReadLine());
        }
        Console.WriteLine(remain);
    }
}
