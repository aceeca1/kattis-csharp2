﻿using System;

class Qlastfactorialdigit {
    public static void Main() {
        var n = int.Parse(Console.ReadLine());
        for (int i = 0; i < n; ++i) {
            var m = int.Parse(Console.ReadLine());
            var answer = 1;
            for (int j = 1; j <= m; ++j) answer *= j;
            Console.WriteLine(answer % 10);
        }
    }
}
