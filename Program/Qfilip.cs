﻿using System;

class filip {
    static string Reverse(string s) {
        var a = s.ToCharArray();
        Array.Reverse(a);
        return new string(a);
    }

    public static void Main() {
        var line = Console.ReadLine().Split();
        var a1 = int.Parse(Reverse(line[0]));
        var a2 = int.Parse(Reverse(line[1]));
        Console.WriteLine(Math.Max(a1, a2));
    }
}