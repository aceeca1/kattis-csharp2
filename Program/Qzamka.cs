﻿using System;

class Qzamka {
    static int DigitSum(int n) {
        int answer = 0;
        while (n != 0) {
            answer += n % 10;
            n /= 10;
        }
        return answer;
    }

    public static void Main() {
        var l = int.Parse(Console.ReadLine());
        var d = int.Parse(Console.ReadLine());
        var x = int.Parse(Console.ReadLine());
        var min = int.MaxValue;
        var max = int.MinValue;
        for (int i = l; i <= d; ++i) {
            if (DigitSum(i) != x) continue;
            if (i < min) min = i;
            if (max < i) max = i;
        }
        Console.WriteLine(min);
        Console.WriteLine(max);
    }
}