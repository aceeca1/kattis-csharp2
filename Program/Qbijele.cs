﻿using System;

class Qbijele {
    static int[] Expected = { 1, 1, 2, 2, 2, 8 };

    public static void Main() {
        var input = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
        for (int i = 0; i < input.Length; ++i) {
            input[i] = Expected[i] - input[i];
        }
        Console.WriteLine(string.Join(" ", input));
    }
}