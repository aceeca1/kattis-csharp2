﻿using System;

class Qprovincesandgold {
    public static void Main() {
        var line = Console.ReadLine().Split();
        var g = int.Parse(line[0]);
        var s = int.Parse(line[1]);
        var c = int.Parse(line[2]);
        var money = g + g + g + s + s + c;
        string victory = null;
        if (money >= 8) victory = "Province";
        else if (money >= 5) victory = "Duchy";
        else if (money >= 2) victory = "Estate";
        string treasure = null;
        if (money >= 6) treasure = "Gold";
        else if (money >= 3) treasure = "Silver";
        else treasure = "Copper";
        if (victory == null) Console.WriteLine(treasure);
        else Console.WriteLine("{0} or {1}", victory, treasure);
    }
}
